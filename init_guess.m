
% In this file we define our initial guesses for every nlp step  

import casadi.*

h_estimate  = l/(Vm*npts);
s_estimate  = s_f*Vm/l;
delta_s_est = s_f/npts;

if(k > 0)
   
    % k guess on curve paramether
    s_guess = k*delta_s_est;
    s_guess_1 = (k-1)*delta_s_est;

    tk      = f_Tnurbs(s_guess);
    tk_1    = f_Tnurbs(s_guess_1);
    
    % k-guess on psi
    psi_guess       = full(atan2(tk(2),tk(1)));
    psi_guess_k_1   = full(atan2(tk_1(2),tk_1(1)));
    
    if(psi_guess > 0)
        psi_guess = -2*pi + psi_guess;
    end
    
    if(psi_guess_k_1 > 0)
        psi_guess_k_1 = -2*pi + psi_guess_k_1;
    end
    
    % k and k-1 guesses on car speed
    u_v_guess     = full([cos(psi_guess) -sin(psi_guess); ...
        sin(psi_guess) cos(psi_guess)]'*tk*Vm);
    
    u_v_guess_k_1 = full([cos(psi_guess_k_1) -sin(psi_guess_k_1); ...
        sin(psi_guess_k_1) cos(psi_guess_k_1)]'*tk_1*Vm);
    
    % k guess on yaw rate
    r_guess = (psi_guess - psi_guess_k_1)/(npts*h_estimate);
    
    % k guess on car position
    x_y_guess = full(f_nurbs(s_guess));
    
    % k guess on Fx
    Fx_guess = m*(u_v_guess(1) - u_v_guess_k_1(1))/(npts*h_estimate);
    
    % k guess on Fy
    Fy_guess = sqrt(mu_y^2*((Fx_guess/mu_x)^2 - Fz^2) + 0.5);
    
    % k state-guess vector
    X_0 = [psi_guess r_guess u_v_guess' x_y_guess']';
    
    % k inputs-guess vector
    U_0 = [rp_0 Fx_guess]';
    
    % k paramethers-guess vector
    Z_0 = [Fy_guess s_guess 0 hm 0]';
        
end