
% This function computes the non-zeros nurbs basis functions for a given
% parameter s, according to formula 2.5 
%
% Inputs list:
%       - (m-1) : number of knots;
%       - N     : matrix where we store all basis function of every order 
%                 columnwise;
%       - p     : degree of nurbs.


function [N_basis] = nurbs_basis_functions(u,p,knots)

import casadi.*

m = length(knots)-1;
N = SX.zeros(m,p+1);

% Basis functions of 0-th order (first part of formula 2.5);
% note the use of if_else (CasAdi function)
for j= 1:m
    N(j,1) = if_else(u < knots(j+1), if_else(u >= knots(j), 1, 0), 0);
end

% Basis functions of higher order (second part of formula 2.5)
for k = 1:p
    for i = 1:m-k
        den1 = (knots(i+k)-knots(i));
        den2 = (knots(i+k+1)-knots(i+1));
        if( den1 == 0 && den2 == 0)
            N(i,k+1) = 0;
        elseif(den1 == 0 && den2 ~= 0) 
              N(i,k+1) = ((knots(i+k+1)-u)/den2)* N(i+1,k);
        elseif(den1 ~= 0 && den2 == 0)
               N(i,k+1) = ((u-knots(i))/den1)*N(i,k);
        else
            N(i,k+1) = ((u-knots(i))/den1)*N(i,k)+...
                ((knots(i+k+1)-u)/den2)* N(i+1,k);
        end
    end
end

N_basis = N;
end
