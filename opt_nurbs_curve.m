
% In this file we define our nlp and run the optimization problem with 
% CasAdi syntax; 
%
% run FIRST file track_construction, so to compute ONLY ONCE the 
% control points for the nurbs track.  

import casadi.*

%% Initialization of NLP

% Load car model
model_for_nurbs_track;                                

% Objective term
L = h^2 + delta_rp^2;

% Continuous time dynamics
f = Function('f', {X, U, Z}, {X_dot, L});

% Definition of collocation points
col_points;

% start an epty NLP
w   = {};
w0  = [];
lbw = [];
ubw = [];

g   = {};
lbg = [];
ubg = [];

J   = 0;

% "Lift" initial conditions
X0  = MX.sym('X0', nx);
w   = {w{:}, X0};
lbw = [lbw; X_0];
ubw = [ubw; X_0];
w0  = [w0; X_0];

Xk = X0;                           

%% B-spline parametric curve function

% Degree of nurbs 
p = 3;

% Compute non-zeros nurbs basis functions for a given s
Nnz      = nurbs_basis_functions(s,p,knots);

% Compute position on nurbs curve for a given s
X_nurbs  = nurbs(Nnz,P,p);

% Compute derivative of position on nurbs curve for a given s
Nd       = nurbs_deriv(P,knots,p,tol);

% Compute tangent VERSOR of position on nurbs curve for a given s
T_nurbs  = deriv_curv_sym_cas(Nnz,Nd,p,tol);

% Define casadi functions for nurbs constraint to run in the NLP 
f_nurbs  = Function('f_nurbs',{Z},{X_nurbs});
f_Tnurbs = Function('f_Tnurbs',{Z},{T_nurbs});

%% Formulate the NLP
%
% remember that :
%           - X = [psi r u v x0 y0]';
%           - U = [rp Fx]';
%           - Z = [Fy s width h delta_rp]'

npts  = 300;

for k = 0:npts    
    
    % Define new guesses for step k
    init_guess;
    
    % New NLP variable for the control vector
    Uk  = MX.sym(['U_' num2str(k)], nu);      
    w   = {w{:}, Uk};
    lbw = [lbw; U_lb];
    ubw = [ubw; U_ub];
    w0  = [w0; U_0];

    % New NLP parameter for the parameter vector
    Zk  = MX.sym(['Z_' num2str(k)], np); 
    w   = {w{:}, Zk};
    lbw = [lbw; Z_lb];
    ubw = [ubw; Z_ub];
    w0  = [w0; Z_0];
    
    % State at collocation points
    Xkj = {};
    for j = 1:d
        Xkj{j}  = MX.sym(['X_' num2str(k) '_' num2str(j)], nx);
        w       = {w{:}, Xkj{j}};
        lbw     = [lbw; X_lb];
        ubw     = [ubw; X_ub];
        w0      = [w0; X_0];
    end
    
    % Loop over collocation points
    Xk_end = D(1)*Xk;
    
    for j=1:d
       % Expression for the state derivative at the collocation point
       xp = C(1,j+1)*Xk;
       for r=1:d
           xp = xp + C(r+1,j+1)*Xkj{r};
       end

       % Append collocation equations
       [fj, qj] = f(Xkj{j}, Uk, Zk);
       g        = {g{:}, Zk(4)*fj - xp};
       lbg      = [lbg; zeros(nx,1)];
       ubg      = [ubg; zeros(nx,1)];

       % Add contribution to the end state
       Xk_end = Xk_end + D(j+1)*Xkj{j};
             
       % Add contribution to quadrature function
       J = J + B(j+1)*qj*Zk(4);
    end
    
    % New NLP variable for state at end of interval 
    Xk  = MX.sym(['X_' num2str(k+1)], nx);
    w   = {w{:}, Xk};
    lbw = [lbw; X_lb];
    ubw = [ubw; X_ub];
    w0  = [w0; X_0];

    % Constraints
    
    % Continuity constraints
    g   = {g{:}, Xk_end-Xk};
    lbg = [lbg; zeros(nx,1)];
    ubg = [ubg; zeros(nx,1)];
 
    % Adherence constraint
    g   = {g{:}, (Uk(2)/(mu_x*Fz))^2 + (Zk(1)/(mu_y*Fz))^2 - 1};
    lbg = [lbg; -inf];
    ubg = [ubg; 0];
    
    % Bounded path-following constraint 
    g   = {g{:}, [Xk(5); Xk(6)] - f_nurbs(Zk(2)) - ...
            Zk(3)*[0 -1; 1 0]*f_Tnurbs(Zk(2))};
    lbg = [lbg; -0.; -0.];
    ubg = [ubg; 0.; 0.];

    
    if(k > 0)
        
        % Parameter vector at previous step
        Z_1  = w{3+(k-1)*4};
        
        % Equality constraint on h_k
        Zk_1 = Z_1(4,1);
        
        g   = {g{:}, Zk(4) - Zk_1};
        lbg = [lbg; 0];
        ubg = [ubg; 0];
        
        % Increasing arc lenght constraint
        Zk_2 = Z_1(2,1);
        
        g   = {g{:}, Zk(2) - Zk_2};
        lbg = [lbg; 0.];
        ubg = [ubg; 1];
        
        % Input vector at previous step
        U_1  = w{2+(k-1)*4};
        
        % Costraint on control smoothness (rp)
        Uk_1 = U_1(1,1);
        
        g   = {g{:}, Zk(5) - (Uk(1) - Uk_1)};
        lbg = [lbg; 0];
        ubg = [ubg; 0];
        
    end   
       
end

% Terminal state constraint

g   = {g{:}, Zk(2)-s_f};
lbg = [lbg; -0.];
ubg = [ubg; 0.];
        
%% Create an NLP solver

prob    = struct('f', J, 'x', vertcat(w{:}), 'g', vertcat(g{:}));
solver  = nlpsol('solver', 'ipopt', prob);

%% Solve the NLP

sol     = solver('x0', w0, 'lbx', lbw, 'ubx', ubw,'lbg', lbg, 'ubg', ubg);
w_opt   = full(sol.x);
L_opt   = full(sol.f);

%% Plot resulting optimal trajectory

variable_plots_nurbs;










