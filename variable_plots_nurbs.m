
% In this file we plot all optimal variables of interest from the
% optimization

%% Get the optimal state from 'w' vector 

% optimal states
psi_opt = w_opt(1:(nx+nu+np)+nx*d:end);
r_opt   = w_opt(2:(nx+nu+np)+nx*d:end);
u_opt   = w_opt(3:(nx+nu+np)+nx*d:end);
v_opt   = w_opt(4:(nx+nu+np)+nx*d:end);
x_opt   = w_opt(5:(nx+nu+np)+nx*d:end);
y_opt   = w_opt(6:(nx+nu+np)+nx*d:end);

% optimal inputs
rp_opt  = w_opt(7:(nx+nu+np)+nx*d:end);
Fx_opt  = w_opt(8:(nx+nu+np)+nx*d:end);

% optimal alg. par.
Fy_opt       = w_opt(9:(nx+nu+np)+nx*d:end);
s_opt        = w_opt(10:(nx+nu+np)+nx*d:end);
spes_opt     = w_opt(11:(nx+nu+np)+nx*d:end);
h_opt        = w_opt(12:(nx+nu+np)+nx*d:end);
delta_rp_opt = w_opt(13:(nx+nu+np)+nx*d:end);

% simulation time
time = (0:npts)*h_opt(1);

%% Track and optimal trajectory plots 

for i = 1:length(Cp)-10
    xy_max(:,i) = Cp(:,i)+nt(:,i)*width_track/norm(nt(:,i));
    xy_min(:,i) = Cp(:,i)-nt(:,i)*width_track/norm(nt(:,i));
end

figure, hold on,
title('Trajectory'),

%plot(Cp(1,:),Cp(2,:),'g-','LineWidth',1.5)

for i = 1:10:length(Cp)
    hold on,
    plot(xy_max(1,:), xy_max(2,:),'k-','LineWidth',1.5)
    plot(xy_min(1,:), xy_min(2,:),'k-','LineWidth',1.5)
end

hold on
for i = 1:10:npts+2
    plot(x_opt(1:i),y_opt(1:i), 'r-.','LineWidth',2); 
    pause(0.05);
end

hold off,
legend({'Middle Track','Upper bound of track', 'Lower bound of track',...
        'Optimal Track'},'Location','SE')
    
figure
quiver(x_opt,y_opt,cos(psi_opt).*u_opt-sin(psi_opt).*v_opt,sin(psi_opt).*u_opt+cos(psi_opt).*v_opt,0.5);
title('Velocity Field')

%% Inputs plots

figure;
subplot(2,1,1)
stairs(time,Fx_opt,'k.-','LineWidth',2);
xlabel('s'), ylabel('N')
Tstring1 = sprintf(...
    'Longitudinal Force (input 1) [npts = %0.0f; s_f = %0.3f]', npts, s_f);
title(Tstring1)

subplot(2,1,2)
stairs(time,rp_opt,'k.-','LineWidth',2);
xlabel('s'), ylabel('rad/s^2')
Tstring2 = sprintf(...
    'Angular acc. (input 2) [npts = %0.0f; s_f = %0.3f]', npts, s_f);
title(Tstring2)

%% Adherence condition plot

figure, hold on
title('Adherence')
stairs(time,(Fx_opt/mu_x).^2+(Fy_opt/mu_y).^2,'b.-','LineWidth',1.5),
plot(time,Fz^2*ones(1,length(time)),'r--','LineWidth',2)
xlabel('s'), ylabel('N'), 
legend({'(F_x/\mu_x)^2 + (F_y/\mu_y)^2','F_z^2'},'Location','SE');


%% Optimal states plots
figure, 
subplot(2,2,1)
plot(time,r_opt(1:end-1),'k','LineWidth',1.5),
xlabel('s'), ylabel('rad/s'), title('r_{opt}')

subplot(2,2,3)
plot(time,psi_opt(1:end-1),'k','LineWidth',1.5),
xlabel('s'), ylabel('rad'), title('\psi_{opt}')

subplot(2,2,2)
plot(time,u_opt(1:end-1),'k','LineWidth',1.5),
xlabel('s'), ylabel('m/s'), title('u_{opt}')

subplot(2,2,4)
plot(time,v_opt(1:end-1),'k','LineWidth',1.5),
xlabel('s'), ylabel('m/s'), title('v_{opt}')

figure,
plot(time,...
    sqrt((cos(psi_opt(1:end-1)).*u_opt(1:end-1) - sin(psi_opt(1:end-1)).*v_opt(1:end-1)).^2 ... 
    + (sin(psi_opt(1:end-1)).*u_opt(1:end-1) + cos(psi_opt(1:end-1)).*v_opt(1:end-1)).^2),'b-.');
xlabel('s'), ylabel('m/s'), title('V_{opt}')

