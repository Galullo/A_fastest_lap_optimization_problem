
% In this function we compute nurbs derivatives, according to formula 3.4
%
% Outputs list:
%           - Qi     : expression of of control point derivative;
%           - Qin    : normalized points of Qi
%           - dknots : new set of knots points;
%
% Inputs list:
%           - Pi    : set of control points;
%           - knots : vector of knots points;
%           - p     : degree of nurbs;

function [Qin, dknots] = nurbs_deriv(Pi,knots,p,tol)

m   = length(knots);
dPx = diff(Pi(:,1));
dPy = diff(Pi(:,2));

Q = zeros(m-2,2);

for i = 1:m-2-p
    
    den = (knots(i+p+1)-knots(i+1));
    
    if (den < tol)
        den = tol;
    end
    
    Q(i,1) = p*dPx(i)/den;
    Q(i,2) = p*dPy(i)/den;
    
end

Qin = Q;
% See formula 3.5
dknots = knots(2:end-1);

end

