
% In this file we define all necessaries parameters for our car point-mass 
% model 

import casadi.*

%% Numeric parameters for car dynamic 

G           = 9.81;
m           = 1;                        % mass of car in kg
Fz          = m*G;                      % weigth force (Newton)
mu_x        = 1.7;                      % dry road example
mu_y        = 1.7;                      % dry road example
width_track = 10;                       % half width of track
tol         = 10^-3;                    % tolerance for avoiding NaN
Vm          = 22.23;                    % mean total speed (m/s) ~ 80 km/h
hm          = .25;                      % mean discretization 
Vi          = Vm;                       % initial speed
Vmax        = 100;                      % max speed
Fx_start    = 3*m*G*Vi/Vmax;            % initial input force
Fy_start    = sqrt(mu_y^2*((Fx_start/mu_x)^2 - Fz^2) + 0.5);

%% Definition of states in CasAdi syntax 

% heading angle
psi     = SX.sym('psi');            
psi_lb  = -2*pi;
psi_ub  = 2*pi;

% yaw rate
r       = SX.sym('r');              
r_lb    = -0.4;
r_ub    = 0.4;

% longitudinal velocity in body frame
u       = SX.sym('u');              
u_lb    = 0;
u_ub    = Vmax;

% lateral velocity in body frame
v       = SX.sym('v');              
v_lb    = -20;
v_ub    = 20;

% x position in inertial frame (S0)
x0      = SX.sym('x0');             
x0_lb   = -inf;
x0_ub   = inf;

% y position in inertial frame (S0)
y0      = SX.sym('y0');             
y0_lb   = -inf;
y0_ub   = inf;

%% Definition of algebraic parameters in CasAdi syntax

% lateral force
Fy      = SX.sym('Fy');             
Fy_lb   = -inf;
Fy_ub   = inf;

% parametrization of curve
s      = SX.sym('s');               
s_lb   = 0.;
s_ub   = 0.9980;

% distance from middle track
width       = SX.sym('width');      
width_lb    = -width_track;
width_ub    = width_track;

% control discretization
h       = SX.sym('h');
h_lb    = 0.;
h_ub    = 0.5;

% rp rate of change
delta_rp        = SX.sym('delta_rp');
delta_rp_lb     = -1.5;
delta_rp_ub     = 1.5;

%% Definition of inputs in CasAdi syntax 

% longitudinal force 
Fx      = SX.sym('Fx');              
Fx_lb   = -4*m*G;
Fx_ub   = 3*m*G;

% angular acceleration (rad/s^2)
rp      = SX.sym('rp');             
rp_lb   = -3;
rp_ub   = 3;

%% Car's dynamic (system of ODEs)

psi_dot = r;
r_dot   = rp;
u_dot   = Fx/m + v*r;
v_dot   = Fy/m - u*r;
x0_dot  = cos(psi)*u - sin(psi)*v;
y0_dot  = sin(psi)*u + cos(psi)*v;

%% Definition of states, inputs and parameters vectors 

nx      = 6;                                          % number of states
X       = [psi r u v x0 y0]';
X_lb    = [psi_lb r_lb u_lb v_lb x0_lb y0_lb]';       % lower bounds
X_ub    = [psi_ub r_ub u_ub v_ub x0_ub y0_ub]';       % upper bounds

nu      = 2;                                          % number of inputs
U       = [rp Fx]';
U_lb    = [rp_lb Fx_lb]';                             % lower bounds
U_ub    = [rp_ub Fx_ub]';                             % upper bounds

np      = 5;                                          % number of alg. par.
Z       = [Fy s width h delta_rp]';
Z_lb    = [Fy_lb s_lb width_lb h_lb delta_rp_lb]';    % lower bounds
Z_ub    = [Fy_ub s_ub width_ub h_ub delta_rp_ub]';    % upper bounds

X_dot = [psi_dot r_dot u_dot v_dot x0_dot y0_dot]';

%% Definition of initial and terminal conditions

% states
psi_0   = 0; 
r_0     = 0; 
u_0     = Vi; 
v_0     = 0; 
x0_0    = Cp(1,1); 
y0_0    = Cp(2,1); 

X_0     = [psi_0 r_0 u_0 v_0 x0_0 y0_0]';                           

% inputs
rp_0    = 0; 
Fx_0    = Fx_start;
U_0     = [rp_0 Fx_0]';

% parameters
Fy_0        = Fy_start;
s_0         = 0;
width_0     = 0;
h_0         = hm;
delta_rp_0  = 0;
Z_0         = [Fy_0 s_0 width_0 h_0 delta_rp_0]';

% terminal condition on curve parameter
s_f = 0.9980;













