
% In this file we construct the numeric nurbs that depict our track from 
% raw data taken from GoogleEarth; in this case we use the configuration 
% between '94-'95 of Silverstone track.

%% Load trajectory from data 

load('silverstone_race.mat');
% raw conversion from latitude and longitude in km
XY = Silverstone'*111e3;
% refining initial points
xy = XY(:,4:end-10);
% closing the track
xy(:,1) = xy(:,end); 

clear Silverstone,

%% Traslate the curve in the center of frame (this is not necessary)

Mx = max(xy(1,:));
My = max(xy(2,:));

for i = 1:length(xy)
    XY_trasl(1,i) = xy(1,i) - Mx;
    XY_trasl(2,i) = xy(2,i) - My;
end 

xy = XY_trasl;
Mx = max(xy(1,:));
mx = min(xy(1,:));

My = max(xy(2,:));
my = min(xy(2,:));

for i = 1:length(xy)
    XY_trasl(1,i) = xy(1,i) - 0.5*mx; %0.5*mx
    XY_trasl(2,i) = xy(2,i) - 0.5*my;   %0.5*my
end 

xy = XY_trasl;

%% Define the variables necessary for the nurbs construction

% li/l : normalized arc length between points i-1 and i (see formula 9.6)
[l, li] = arclength(xy(1,:),xy(2,:),'linear');

% define knots vector; we use the recommended technique of "averaging"
% (see formula 9.8)
knots2  = zeros(1, length(li)+1);

for i = 1:length(li)
    knots2(i+1) = knots2(i)+li(i)/l;
end

knots = [0 0 0 knots2(1:4:end-1) 1 1 1 1]; 

% define control points vector
ctrl_pts = bspline_estimate(4,knots,knots2,xy);

% close curve with continuity C^2 (see page 572)
P           = ctrl_pts;
P(:,end-2)  = P(:,1);
P(:,end-1)  = P(:,2);
P(:,end)    = P(:,3);

%% Numeric Nurbs construction

C   = bspline_deboor(4,knots,ctrl_pts);
Cp  = bspline_deboor(4,knots,P);

%% Derivative path construction

[dknots, dcntrl] = bspline_deriv(4,knots,P);
% tangent vector
dC               = bspline_deboor(3,dknots,dcntrl);
% normal vector
nt               = [0 -1; 1 0]*dC;

% normalize normal vector
for i = 1:length(dC)
    dC_n(:,i)   = dC(:,i)/norm(dC(:,i));
    nt(:,i)     = nt(:,i)/norm(nt(:,i)); 
end

%% Plot obtained curves

figure,
hold on,
plot(xy(1,:),xy(2,:),'bo'),
plot(Cp(1,:),Cp(2,:),'r.-')

for i = 1:length(Cp)-10
    plot([Cp(1,i) Cp(1,i)+dC_n(1,i)],[Cp(2,i) Cp(2,i)+dC_n(2,i)],'k-^'),
    plot([Cp(1,i) Cp(1,i)+nt(1,i)],[Cp(2,i) Cp(2,i)+nt(2,i)],'m-^')
end

hold off, 
legend('point curve','Bspline evaluete','tangent Bspline',...
        'normal Bspline','Location', 'SE')
title('Frenet''s frame')

figure,
hold on,
plot(xy(1,:),xy(2,:),'b*','LineWidth',1.5),
plot(ctrl_pts(1,:), ctrl_pts(2,:),'kx');
text(ctrl_pts(1,:), ctrl_pts(2,:),[repmat('  ',max(length(ctrl_pts)),1),...
        num2str((1:max(length(ctrl_pts)))')])
plot(C(1,:),C(2,:),'r.-','LineWidth',2)
plot(Cp(1,:),Cp(2,:),'g.-','LineWidth',2)
hold off, 
legend('points of path','control points','B-spline',...
        'B-spline with ctrlpts adjustment','Location', 'SE')
title('Construction of Nurbs Track')

%% clear un-used variables

clear XY_trasl, clear My, clear my, clear Mx, clear mx,

% necessary for run others nurbs function
P = P';             








