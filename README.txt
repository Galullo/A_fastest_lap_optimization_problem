﻿FINAL VERSION OF POINT MASS CAR MODEL WITH NURBS TRACK



First of all, add “bspline” folder path with subfolders.


Run first “trajectory_construction.m”, so to compute nurbs track only once. 


Then it is possible to run the optimization problem contained in “opt_nurbs_curve.m”. 


All mentioned formulas are taken from “The NURBS Book”.



The “bspline” and “arc_length.m” code are taken from Mathworks website.


There is a slight modification in “bspline_estimate.m” for better estimation of control points.
