
% This function computes our nurbs according to formula 3.1
%
% Inputs list:
%  - p : degree of nurbs
%  - N : vector of p-degree non-zeros basis functions (output of 
%        file nurbs_basis_functions)
%  - Pi : vector of control points (output of file nurbs_track)

function [curva] = nurbs(N,Pi,p)

import casadi.*

n       = max(length(Pi));
C       = N(1:n,p+1)'*Pi;
curva   = C';
end
