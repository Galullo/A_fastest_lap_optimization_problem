
% In this function we obtain the symbolic espression of curve derivative 
%
% Inputs list:
%           - N   : matrix where we store all basis of every order 
%                   columwise
%           - Qi  : expression of control point derivative;
%           - p   : degree of nurbs

function [d_curva] = deriv_curv_sym_cas(N,Qi,p,tol)

import casadi.*

% See formula 3.6
n = max(length(Qi));
dC = N(1:n,p)'*Qi;
d_curva = dC';

d_curva = if_else(norm(d_curva) > tol, d_curva/norm(d_curva), d_curva/tol);
%d_curva = d_curva/norm(d_curva);
end

